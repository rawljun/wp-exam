<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package PhotoUp 2020
 */

get_header();
?>
<section class="page-section error-404 not-found">
	<div class="container text-center pt-100">
		<header class="page-header">
			<h1 class="page-title">Oops! That page can&rsquo;t be found</h1>
		</header><!-- .page-header -->

		<div class="page-content">
			<p>It looks like nothing was found at this location.</p>
		</div><!-- .page-content -->
	</div>
</section>

<?php
get_footer();