jQuery(function ($) {
	'use strict';

	$.PhotoUpAdminScript = {
		init: function () {
			console.log('loaded custom admin script init');

			this.removeAuthorProfile();
			this.uploadAuthorProfile();
		},

		uploadAuthorProfile: function() {

		    $('body').on('click', '.upload_profile', function(e){
		        e.preventDefault();

		         var button = $(this),
		            custom_uploader = wp.media({
		            title: 'Insert image',
		            library : {
		                // uncomment the next line if you want to attach image to the current post
		                // uploadedTo : wp.media.view.settings.post.id, 
		                type : 'image'
		            },
		            button: {
		                text: 'Use this image' // button label text
		            },
		            multiple: false // for multiple image selection set to true
		        }).on('select', function() { // it also has "open" and "close" events 
		            var attachment = custom_uploader.state().get('selection').first().toJSON();
		            $(button).html('<img class="true_pre_image" src="' + attachment.url + '" style="max-width:95%;display:block;" />')
		            				.next().val(attachment.id)
		            				.next().show();
		            $(button).html('<img class="true_pre_imag" src="' + attachment.url + '" style="max-width:95%;display:block;" />')
		            				.next().val(attachment.id)
		            				.next().show(); 
		        })
		        .open();
		    });
		},
		removeAuthorProfile: function() {

		    $('body').on('click', '.remove_profile', function(){
		        $(this).hide().prev().val('').prev().html('Set author profile');
		        return false;
		    });
		},
		
	}
	//initialize script
  	$.PhotoUpAdminScript.init();
});