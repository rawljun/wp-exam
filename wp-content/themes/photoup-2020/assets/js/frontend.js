jQuery(function ($) {
  'use strict';

  $.PhotoUpFrontendScript = {
    init: function () {
      
      this.showTestimonials();
    },

    showTestimonials: function() {

      $('.btn-more').on('click', function(){

          window.location.href = photoup.testimonial_page;
      });
    }
    
  }
  //initialize script
    $.PhotoUpFrontendScript.init();
});