<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'PhotoUp_Customize' ) ) {
	class PhotoUp_Custom_Post_Type {

		public static function init() {
			add_action( 'init', array( __CLASS__, 'register_custom_post_type' ) );

			//override testimonial custom post type table display
			add_filter('manage_testimonials_posts_columns' , array( __CLASS__, 'testimonials_cpt_columns' ) );
			add_filter('manage_testimonials_posts_columns' , array( __CLASS__, 'set_testimonial_columns' ));
			add_action( 'manage_testimonials_posts_custom_column' , array( __CLASS__, 'display_testimonials_column' ), 10, 2 );
		}

		public static function register_custom_post_type() {
			if ( post_type_exists( 'testimonials' ) ) {
				return;
			}

			$supports = array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields' );

			register_post_type(
				'testimonials',
				array(
					'labels'              => array(
						'name'                => _x( 'Testimonial', 'Post Type General Name', 'photoup-2020' ),
				        'singular_name'       => _x( 'Testimonial', 'Post Type Singular Name', 'photoup-2020' ),
				        'menu_name'           => __( 'Testimonials', 'photoup-2020' ),
				        'parent_item_colon'   => __( 'Parent Testimonials', 'photoup-2020' ),
				        'all_items'           => __( 'Testimonials', 'photoup-2020' ),
				        'view_item'           => __( 'View Testimonials', 'photoup-2020' ),
				        'add_new_item'        => __( 'Add New Testimonial', 'photoup-2020' ),
				        'add_new'             => __( 'Add New', 'photoup-2020' ),
				        'edit_item'           => __( 'Edit Testimonial', 'photoup-2020' ),
				        'update_item'         => __( 'Update Testimonial', 'photoup-2020' ),
				        'search_items'        => __( 'Search Testimonials', 'photoup-2020' ),
				        'not_found'           => __( 'Not Found', 'photoup-2020' ),
				        'not_found_in_trash'  => __( 'Not found in Trash', 'photoup-2020' ),
					),
					'description'         => __( 'This is where you can add new testimonials.', 'photoup-2020' ),
			        'supports'            => $supports,
			        'hierarchical'        => false,
			        'public'              => true,
			        'show_ui'             => true,
			        'show_in_menu'        => true,
			        'show_in_nav_menus'   => true,
			        'show_in_admin_bar'   => true,
			        'menu_position'       => 5,
			        'can_export'          => true,
			        'has_archive'         => true,
			        'exclude_from_search' => false,
			        'publicly_queryable'  => true,
			        'capability_type'     => 'page',
			        'menu_icon'           => 'dashicons-welcome-widgets-menus'

				)
			);

		}

		public static function testimonials_cpt_columns($columns) {
			unset(
				$columns['comments'],
				$columns['author']
			);
			$new_columns = array(
					'testimony'     =>__('Content'),
					'testifier'     =>__('Author'),
	        		'rating' 		=>__( 'Ratings')
			);
		    return array_merge($columns, $new_columns);
		}

		public static function set_testimonial_columns($columns) {
		    return array(
		        'cb' 			=> '<input type="checkbox" />',
		        'title' 		=> __('Title'),
		        'testimony' 	=>__( 'Content'),
		        'rating' 		=>__( 'Ratings'),
		        'testifier' 	=>__( 'Author'),
		        'date' 			=> __('Date')
		    );
		}

		public static function display_testimonials_column( $column, $post_id ) {
		    switch ( $column ) {

				case 'testimony' :
						echo apply_filters('the_content', get_post_field('post_content', $post_id));
						break;

				case 'testifier' :
						echo get_post_meta($post_id, '_testimonial_author', true);
						break;

				case 'rating' :
						echo get_post_meta($post_id, '_testimonial_rating', true);
						break;
			}
		}

	}
	PhotoUp_Custom_Post_Type::init();
}