<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
 
class Admin_Custom_Menu {
    
    function __construct() {
    
       add_action('admin_menu', array($this, 'custom_option_menu') );
    }

    function custom_option_menu() {

      $menu_icon_url = 'dashicons-admin-generic';

      add_menu_page(__('PhotoUp Settings', 'photoup-2020'), __('PhotoUp Settings', 'photoup-2020'), 'manage_options', 'photoup-menu-settings' , array($this, 'content_menu_settings'), $menu_icon_url, 9);

    }

    function content_menu_settings() {
      if (!current_user_can('manage_options')) {
        wp_die('You do not have permission to access this settings page.');
      }

        $message = '';
        if( isset($_REQUEST['menu_settings']) ) {
            $nonce = $_REQUEST['_wpnonce'];

            if ( !wp_verify_nonce($nonce, 'photoup_menu_settings')){
                wp_die('Error! Nonce Security Check Failed! Save the settings again.');
            }
      
            update_option('testimonial_option', $_REQUEST['testimonial_page']);

            $message .= '<div id="message" class="updated fade">';
            $message .= '<p>'.__('PhotoUp Settings successfully updated!', 'photoup-2020').'</p>';
            $message .= '</div>';

        }
     
        $testimonial_option = get_option('testimonial_option');  
        ?>
        <div class="wrap">
            <div class="poststuff">
                <div class="post-body">
                    <?php echo $message; ?>
                    <form method="post" action="">
                        <?php wp_nonce_field('photoup_menu_settings'); ?>
                        <h1><?php _e('PhotoUp Settings'); ?></h1>
                        <table class="form-table">
                            <tbody>
                                <tr class="form-field">
                                    <th scope="row">
                                        <label for="role"><?php _e('All Testimonials','photoup-2020'); ?></label>
                                    </th>
                                    <td>
                                        <select name="testimonial_page"> 
                                         <option value="">Select page</option> 
                                         <?php 
                                          $pages = get_pages(array('exclude' => 67)); 
                                          foreach ( $pages as $page ) {
                                            $selected = '';
                                            if( $page->ID ==  $testimonial_option ){
                                                $selected = 'selected="selected"';
                                            }
                                            $option = '<option value="' .$page->ID . '" '.$selected.'>';
                                            $option .= $page->post_title;
                                            $option .= '</option>';
                                            echo $option;
                                          }
                                         ?>
                                        </select>
                                        <em><?php _e('A page to display all testimonials.', 'photoup-2020'); ?></em>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p class="submit">
                            <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>"/>
                            <input type="submit" name="menu_settings" id="menu-settings" class="button button-primary" value="<?php _e('Save', 'photoup-2020'); ?>">
                        </p>
                    </form>
                </div>
            </div>
        </div>
        <?php

    } 
}
return new Admin_Custom_Menu();