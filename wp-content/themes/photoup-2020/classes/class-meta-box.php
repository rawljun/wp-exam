<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class PhotoUp_Meta_Box {

	public function __construct() {

		add_action( 'add_meta_boxes', array( $this, 'add_photoup_meta_box' ), 30 );
		add_action( 'save_post', array( $this, 'save_meta_box_data' ), 1, 2 );
		add_action( 'admin_notices', array( $this, 'photoup_admin_notices' ) );
	 	add_filter( 'post_updated_messages', array( $this, 'photoup_post_updated_messages') );
	}

	public function add_photoup_meta_box() {

	    add_meta_box(
				'testimonial_rating',          // $id
				'Ratings',         			   // $title
				'Meta_Ratings::output',   	   // $callback
				'testimonials',                // $post_type or page
				'side',                        // $context
				'low'                          // $priority
			);

	    add_meta_box(
				'testimonial_author',          // $id
				'Author',         			   // $title
				'Meta_Author::output',   	   // $callback
				'testimonials',                // $post_type or page
				'side',                        // $context
				'low'                          // $priority
			);
	}

	public function photoup_post_updated_messages( $messages ) {

		$errors = get_option('photoup_admin_errors');

	    if($errors){
	    	return array();
	    }
	    return $messages;
	}

	public function photoup_admin_notices() {
		global $post;

		if($post){

			if($post->post_type == 'testimonials') {

			    $errors = get_option('photoup_admin_errors');

			    if($errors){
			        echo '<div class="notice notice-error is-dismissible"><p>' . $errors . '</p></div>';
			    }
			}
		}
		update_option('photoup_admin_errors', false);
	}

	public function save_meta_box_data( $post_id, $post ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return;

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}

		// check nonce
		if ( !wp_verify_nonce($_POST['photoup_nonce'], 'wp_photoup_nonce' ) ) {
			return;
		}

		if( in_array( $post->post_type, array( 'testimonials' ), true )){
			$errors = false;
			$default_data['_testimonial_rating'] = (isset($_POST['_testimonial_rating'])) ? $_POST['_testimonial_rating'] : '';
			$default_data['_testimonial_author'] = (isset($_POST['_testimonial_author'])) ? $_POST['_testimonial_author'] : '';
			$default_data['_testimonial_author_profile'] = (isset($_POST['_testimonial_author_profile'])) ? $_POST['_testimonial_author_profile'] : 'default';
			
			if( $default_data['_testimonial_rating'] == '' ) {
				$errors .= 'Please input testimonial ratings. <br />';
			}

			if( $default_data['_testimonial_author'] == '' ) {
				$errors .= 'Please input testimonial author full name. <br />';
			}

			//saved  errors
			update_option('photoup_admin_errors', $errors);

			//get errors
			$custom_errors = get_option('photoup_admin_errors', $errors);

			if(!$custom_errors) {
				foreach ( $default_data as $key => $meta ) {

					if ( ! $meta  ) {

						delete_post_meta( $post_id, $key );
					}

					if ( get_post_meta( $post_id, $key, false ) ) {

						update_post_meta( $post_id, $key, $meta );
					} else {

						add_post_meta( $post_id, $key, $meta);
					}
				} 
			}
		}

	}

}
return new PhotoUp_Meta_Box();