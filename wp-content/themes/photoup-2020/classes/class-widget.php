<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class testiomonial_widget extends WP_Widget {

	var $defaults;		// default values
	var $ints;			// key names of integer variables of any value

	function __construct() {
	
		$this->defaults[ 'number_posts' ]		= 5; // number of testimonial to show in the widget

		// other vars
		$this->ints 							= array( 'number_posts');
		$this->bools_true						= array( 'display_all_link' );

		parent::__construct( 'testimonial-widget', 'Testimonials Widget', 'Custom testimonial widget' );

		add_action( 'save_post',				array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post',				array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme',				array( $this, 'flush_widget_cache' ) );
	}

	function widget( $args, $instance ) {
		global $post;

		if ( ! isset( $args[ 'widget_id' ] ) ) {
			$args[ 'widget_id' ] = $this->id;
		}

		// get and sanitize values
		$ints = array();
		foreach ( $this->ints as $key ) {
			$ints[ $key ] = ( ! empty( $instance[ $key ] ) ) ? absint( $instance[ $key ] ) : $this->defaults[ $key ];
		}

		foreach ( $this->bools_true as $key ) {
			$bools[ $key ] = ( isset( $instance[ $key ] ) ) ? (bool) $instance[ $key ] : false;
		}

		// default params
		$query_args = array(
			'posts_per_page'      => $ints[ 'number_posts' ],
			'post_status'         => 'publish',
			'post_type'           => 'testimonials'
		);
		
		//query all posts
		$query_all_args = array(
			'posts_per_page'      => -1,
			'post_status'         => 'publish',
			'post_type'           => 'testimonials'
		);
		
		$testimonials = new WP_Query( $query_all_args );
		$testimonial_count = $testimonials->post_count;
		
		// set order of posts in widget
		$query_args[ 'orderby' ] = 'id';
		$query_args[ 'order' ] = 'DESC';

		// run the query
		$query = new WP_Query( $query_args );

		if( $query->post_count > 1 ):
			//display to carousel for more than 1 testimonial
			if ( $query->have_posts()) : ?>
				<div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<?php 
						$counter = 0;
						while ( $query->have_posts() ) : $query->the_post(); ?>
							<?php 
								$profile_id = get_post_meta($query->post->ID, '_testimonial_author_profile', true);
								$profile 	= wp_get_attachment_image_src( $profile_id, 'full' );
							    $rating 	= get_post_meta( $query->post->ID, '_testimonial_rating', true );
								?>
							    <div class="carousel-item <?php echo ($counter == 0 ) ? 'active' : ''; ?>" data-interval="10000">
									<div class="testimonial-info text-center">
										<div class="rating-<?php echo $rating; ?>">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="145.049" height="25.695" viewBox="0 0 145.049 25.695">
											  <defs>
											    <clipPath id="clip-path">
											      <rect width="145.049" height="25.695" fill="none"/>
											    </clipPath>
											  </defs>
											  <g id="stars" clip-path="url(#clip-path)">
											  	<?php apply_filters( 'star_rating', $rating ); ?>
											  </g>
											</svg>
										</div>

				                        <p>"<?php  echo get_the_content(); ?></p>

				                        <?php if($profile_id): ?>
											<img src="<?php echo $profile['0']; ?>">
										<?php else: ?>
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/default-profile2.png">
										<?php endif; ?>

				                       	<h5><?php echo  get_post_meta($query->post->ID, '_testimonial_author', true) ?></h5>

				                       	<?php if ($bools['display_all_link'] == 1): ?>
				                       		<button class="btn btn-primary btn-more">More Testimonials</button>
				                        <?php endif; ?>

				                    </div>
				                </div>
							<?php
					    $counter++; 
						endwhile;
						
						// Reset post query
						wp_reset_postdata(); ?>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			<?php 
			endif;

		else:
			//for 1 testimonial only
			if ( $query->have_posts()) :
				while ( $query->have_posts() ) : $query->the_post(); ?>
					<?php 
						$profile_id = get_post_meta($query->post->ID, '_testimonial_author_profile', true);
						$profile 	= wp_get_attachment_image_src( $profile_id, 'full' );
					    $rating 	= get_post_meta( $query->post->ID, '_testimonial_rating', true );
						?>
						<div class="testimonial-info text-center">
							<div class="rating-<?php echo $rating; ?>">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="145.049" height="25.695" viewBox="0 0 145.049 25.695">
								  <defs>
								    <clipPath id="clip-path">
								      <rect width="145.049" height="25.695" fill="none"/>
								    </clipPath>
								  </defs>
								  <g id="stars" clip-path="url(#clip-path)">
								  	<?php apply_filters( 'star_rating', $rating ); ?>
								  </g>
								</svg>
							</div>

	                        <p>"<?php  echo get_the_content(); ?></p>

	                        <?php if($profile_id): ?>
								<img src="<?php echo $profile['0']; ?>">
							<?php else: ?>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/default-profile2.png">
							<?php endif; ?>

	                       	<h5><?php echo  get_post_meta($query->post->ID, '_testimonial_author', true) ?></h5>
	                       	<?php if ($bools['display_all_link'] == 1): ?>
	                       		<button class="btn btn-primary btn-more">More Testimonials</button>
	                        <?php endif; ?>
	                    </div>
					<?php 
				endwhile;
				
				// Reset post query
				wp_reset_postdata();

			endif;

		endif;

			
	}

	function update( $new_widget_settings, $old_widget_settings ) {

		$instance = $old_widget_settings;

		// initialize integer variables
		foreach ( $this->ints as $key ) {
			$instance[ $key ] = ( isset( $new_widget_settings[ $key ] ) ) ? absint( $new_widget_settings[ $key ] ) : $this->defaults[ $key ];
		}

		foreach ( $this->bools_true as $key ) {
			$instance[ $key ] = ( isset( $new_widget_settings[ $key ] ) ) ? (bool) $new_widget_settings[ $key ] : false;
		}

		// empty widget cache
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions[ 'testimonial-widget' ] ) ) {
			delete_option( 'testimonial-widget' );
		}

		// return sanitized current widget settings
		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete( 'testimonial-widget', 'widget' );
	}

	function form( $instance ) {

		// initialize integer variables
		$ints = array();
		foreach ( $this->ints as $key ) {
			$ints[ $key ] = ( isset( $instance[ $key ] ) ) ? absint( $instance[ $key ] ) : $this->defaults[ $key ];
		}

		foreach ( $this->bools_true as $key ) {
			$bools[ $key ] = ( isset( $instance[ $key ] ) ) ? (bool) $instance[ $key ] : true;
		}

		// compute ids only once to improve performance
		$field_ids = array();
		foreach ( array_merge($this->ints, $this->bools_true) as $key ) {
			$field_ids[ $key ] = $this->get_field_id( $key );
		}

		// print form in widgets page
		?>	
		<p>
			<label><?php _e('Number of testimonials to display:', 'testimonial-widget') ?></label>
			<input type="number" min="1" name="<?php echo $this->get_field_name( 'number_posts' ); ?>" value="<?php echo $ints[ 'number_posts' ]; ?>" />
		</p>
		<p>
			<input type="checkbox" <?php checked( $bools[ 'display_all_link' ] ); ?> class="checkbox" id="<?php echo $field_ids[ 'display_all_link' ]; ?>" name="<?php echo $this->get_field_name( 'display_all_link' ); ?>" />

			<label for="<?php echo $field_ids[ 'display_all_link' ]; ?>"><?php _e( 'Display All Link?', 'testimonial-widget' ); ?></label>
			<br />
			<em><?php _e( 'If selected and If the total number of testimonial is greater than 1, the "More Testimonials" link button will show up.', 'testimonial-widget' ); ?></em>
		</p>
		<?php 

	}

}

/**
 * Register Testimonial widget
 */
function register_testiomonial_widget () {
	register_widget( 'testiomonial_widget' );
}
add_action( 'widgets_init', 'register_testiomonial_widget', 1 );