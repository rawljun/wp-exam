<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Meta_Author {

	public static function output( $post ) {
		
		$author = get_post_meta($post->ID, '_testimonial_author', true);
		$id = get_post_meta($post->ID, '_testimonial_author_profile', true);

	    ?>
	    	<div class="attachment-info">
	    		<label>Name</label>
				<input type="text" name="_testimonial_author" class="widefat" value="<?php echo $author; ?>">
	    	</div>
	    	<label>Photo</label>
	  		<?php if($id): ?>
				<div class="gallery-holder">
					<?php echo Meta_Author::profile_uploader( '_testimonial_author_profile', $id);?>
				</div>

	    	<?php else: ?>
				<div class="gallery-holder">
					<?php echo Meta_Author::profile_uploader( '_testimonial_author_profile', '');?>
				</div>
	    	<?php endif; ?>
	<?php 
	}

	public static function profile_uploader( $name, $value = ''){

		$image = '">Set author photo';
	    $image_size = 'full'; 
	  	$display = 'none'; 

	    if( $image_attributes = wp_get_attachment_image_src( $value, $image_size ) ) {

	        $image = '"><img src="' . $image_attributes[0] . '" style="max-width:95%;display:block;" />';
	        $display = 'inline-block';
	    } 

	    return '<div class="gallery-grp">
			        <a href="#" class="upload_profile' . $image . '</a>
			        <input type="hidden" name="' . $name . '" value="' . $value . '" />
			        <a href="#" class="remove_profile" style="display:inline-block;display:' . $display . '">Remove photo</a>
			    </div>';

	}
}