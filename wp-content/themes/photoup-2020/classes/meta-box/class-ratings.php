<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Meta_Ratings {

	public static function output( $post ) {

		$default_rating = 5; //set default rating as 5;

	 	$testimonial = get_post_meta( $post->ID, '_testimonial_rating', true );

	 	$rating = ($testimonial > 0) ? $testimonial : $default_rating;

		?>
		<?php echo wp_nonce_field( 'wp_photoup_nonce', 'photoup_nonce' ); ?>
		<label>Ratings</label>
		<select class="widefat" name="_testimonial_rating">
			<option value="1" <?php echo ($rating == 1) ? 'selected="selected"': ''; ?>>1</option>
			<option value="2" <?php echo ($rating == 2) ? 'selected="selected"': ''; ?>>2</option>
			<option value="3" <?php echo ($rating == 3) ? 'selected="selected"': ''; ?>>3</option>
			<option value="4" <?php echo ($rating == 4) ? 'selected="selected"': ''; ?>>4</option>
			<option value="5" <?php echo ($rating == 5) ? 'selected="selected"': ''; ?>>5</option>
		</select>

   	    <?php
	}
}