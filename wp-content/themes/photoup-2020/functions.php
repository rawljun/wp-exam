<?php
/**
 * PhotoUp 2020 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package PhotoUp 2020
 */

if ( ! defined( 'PHOTOUP_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( 'PHOTOUP_VERSION', '1.0.0' );
}

if ( ! function_exists( 'photoup_2020_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function photoup_2020_setup() {

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'photoup-2020' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'photoup_2020_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'photoup_2020_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function photoup_2020_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'photoup_2020_content_width', 640 );
}
add_action( 'after_setup_theme', 'photoup_2020_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function photoup_2020_widgets_init() {

	//This widget is for home page
	register_sidebar(
		array(
			'name'          => esc_html__( 'Home', 'photoup-2020' ),
			'id'            => 'home-widget',
			'description'   => esc_html__( 'Add widgets here.', 'photoup-2020' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	//Default sidebar widgets
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'photoup-2020' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'photoup-2020' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

}
add_action( 'widgets_init', 'photoup_2020_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function frontend_scripts() {

	wp_enqueue_style( 'photoup-2020-style', get_stylesheet_uri(), array(), PHOTOUP_VERSION );

	wp_enqueue_style('bootstrap-min-css', get_stylesheet_directory_uri() . '/assets/css/vendor/bootstrap.min.css?'.time().'', array('photoup-2020-style'), PHOTOUP_VERSION);
	wp_enqueue_style('main-css', get_stylesheet_directory_uri() . '/assets/css/main.css?'.time().'', array('bootstrap-min-css'), PHOTOUP_VERSION);
	
	wp_enqueue_script('jquery-3.5.1.slim.min', get_stylesheet_directory_uri() .'/assets/js/vendor/jquery-3.5.1.slim.min.js?'.time().'', array(), PHOTOUP_VERSION, true);
	wp_enqueue_script('popper.min',  get_stylesheet_directory_uri() .'/assets/js/vendor/popper.min.js?'.time().'', array(), PHOTOUP_VERSION, true);
	wp_enqueue_script('bootstrap.min',  get_stylesheet_directory_uri() .'/assets/js/vendor/bootstrap.min.js?'.time().'', array(), PHOTOUP_VERSION, true);
	wp_enqueue_script('jquery.magnific-popup.min',  get_stylesheet_directory_uri() .'/assets/js/vendor/jquery.magnific-popup.min.js?'.time().'', array(), PHOTOUP_VERSION, true);

	wp_enqueue_script('photoup-frontend-script', get_stylesheet_directory_uri() .'/assets/js/frontend.js?'.time().'', array(), PHOTOUP_VERSION, true);

	wp_localize_script( 'photoup-frontend-script', 'photoup', array( 
			'testimonial_page' 	=> get_page_link( get_option('testimonial_option') ),
		) 
	);
}
add_action( 'wp_enqueue_scripts', 'frontend_scripts' );


function admin_scripts() {
	global $post;

	if($post){

		if($post->post_type == 'testimonials' ){
			
			wp_enqueue_script( 'admin-jquery',get_template_directory_uri().'/assets/js/admin/jquery-1.12.4.js', array( 'jquery' ) );
			wp_enqueue_script( 'admin-js',get_template_directory_uri().'/assets/js/admin/admin.js', array( 'admin-jquery' ) );
		}
	}
	
}

add_action( 'admin_enqueue_scripts', 'admin_scripts' );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

include_once dirname( __FILE__ ). '/classes/class-header-walker.php';
include_once dirname( __FILE__ ). '/classes/meta-box/class-author.php';
include_once dirname( __FILE__ ). '/classes/meta-box/class-ratings.php';
include_once dirname( __FILE__ ). '/classes/class-meta-box.php';

require get_template_directory(). '/classes/class-custom-post-type.php';

require get_template_directory(). '/classes/class-widget.php';

require get_template_directory().  '/classes/class-menu.php';

//remove the admin bar for all users
add_filter('show_admin_bar', '__return_false');

//Hook for testimonial star rating
add_filter( 'star_rating', 'star_rating_filter' );
function star_rating_filter($rating) {
	switch ($rating) {
		case '5':
			echo '<g transform="translate(-667 -2021.203)">
				      <path id="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>
				    <g transform="translate(-638 -2021.203)">
				      <path id="icons8-filled_star-2" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>
				    <g transform="translate(-609 -2021.203)">
				      <path id="icons8-filled_star-3" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>
				    <g transform="translate(-580 -2021.203)">
				      <path id="icons8-filled_star-4" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>
				    <g transform="translate(-551 -2021.203)">
				      <path id="icons8-filled_star-5" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>';
			break;
	    case '4':
			echo '<g transform="translate(-667 -2021.203)">
				      <path id="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>
				    <g transform="translate(-638 -2021.203)">
				      <path id="icons8-filled_star-2" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>
				    <g transform="translate(-609 -2021.203)">
				      <path id="icons8-filled_star-3" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>
				    <g transform="translate(-580 -2021.203)">
				      <path id="icons8-filled_star-4" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>';
			break;
		case '3':
			echo '<g transform="translate(-667 -2021.203)">
				      <path id="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>
				    <g transform="translate(-638 -2021.203)">
				      <path id="icons8-filled_star-2" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>
				    <g transform="translate(-609 -2021.203)">
				      <path id="icons8-filled_star-3" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>';
			break;
		case '2':
			echo '<g transform="translate(-667 -2021.203)">
				      <path id="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>
				    <g transform="translate(-638 -2021.203)">
				      <path id="icons8-filled_star-2" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>';
			break;
		
		default:
			echo '<g transform="translate(-667 -2021.203)">
				      <path id="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
				    </g>';
			break;
	}
}