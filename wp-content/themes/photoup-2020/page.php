<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package PhotoUp 2020
 */
get_header();
?>
<section class="page-section">
	<div class="container pt-100">
		<?php
			if( get_option('testimonial_option') == get_the_ID() ):
				
				$args = array(
					'posts_per_page'      => -1,
					'post_status'         => 'publish',
					'post_type'           => 'testimonials'
				);
				
				$query = new WP_Query( $args );

				if ( $query->have_posts()) :
					while ( $query->have_posts() ) : $query->the_post();

						get_template_part('template-parts/content', 'testimonials');

					endwhile;
				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;

			else:

				while ( have_posts() ) :
					the_post();

						get_template_part( 'template-parts/content', 'page' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
				endwhile; // End of the loop.
			endif;
		?>
	</div>	
</section>
<?php
get_sidebar();
get_footer();