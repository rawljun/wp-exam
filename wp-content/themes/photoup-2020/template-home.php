<?php 
/** 
* Template Name: Home Page template
*/ 
get_header();
?>
	<!-- Banner-->
    <div class="banner">
    	<div class="container">
    		<div class="row">
    			<div class="banner-info">
    				<div class="banner-caption">
    					<span>Quality General Contracting for</span>
        				<h2>Home <span>&</span> Commercial Remodeling Services in Perry, Ohio</h2>
        				<p>No job too small or too big, we can help.</p>
        				<p>We do estimate for free!</p>
        				<button class="btn btn-primary btn-estimate">Request An Estimate</button>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <!-- Section 2-->
    <div class="page-section section-about" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/section-2.png">
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="about-info">
                    	<span>At James Martin Contractor Services</span>
                   	 	<h2 class="pt-lg-2">We help you build your dream.</h2>
                        <p class="about-desc pt-lg-3">Our team is your partner in construction and facility management for companies and individuals who need remodeling or building repairs.</p>
                        <p class="about-additional-info">Need to update your home? We do additions, windows, kitchens, bathrooms, and basements</p>
                        <button class="btn btn-primary btn-get-started">Get Started</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Section 3-->
    <div class="page-section section-team" id="team">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="team-info">
						<svg id="logo" xmlns="http://www.w3.org/2000/svg" width="77.858" height="66.147" viewBox="0 0 77.858 66.147">
						  <path id="Path_19" data-name="Path 19" d="M111.907,75.457h7.6L91.335,124.238H83.661Z" transform="translate(-60.276 -58.091)" fill="#bfa665"/>
						  <path id="Path_20" data-name="Path 20" d="M142.455,75.457h7.6l-28.174,48.782h-7.674Z" transform="translate(-80.051 -58.091)" fill="#bfa665"/>
						  <path id="Path_21" data-name="Path 21" d="M80.44,75.457h7.6L59.869,124.238H52.195Z" transform="translate(-39.906 -58.091)" fill="#bfa665"/>
						  <g id="Group_2" data-name="Group 2" transform="translate(0)">
						    <path id="Path_22" data-name="Path 22" d="M77.073,26.213h7.6L56.5,75H48.827Z" transform="translate(-37.726 -26.213)" fill="#15135f"/>
						    <path id="Path_23" data-name="Path 23" d="M36.922,106.157l-11.9,20.6H17.347l11.922-20.6Z" transform="translate(-17.347 -77.966)" fill="#15135f"/>
						    <path id="Path_24" data-name="Path 24" d="M146.579,33.953,141.4,42.8l1.239,2.75h10.209Z" transform="translate(-97.653 -31.224)" fill="#bcbec0"/>
						  </g>
						  <path id="Path_25" data-name="Path 25" d="M210.846,84.727l-5.179,8.846,1.239,2.75h10.209Z" transform="translate(-139.257 -64.093)" fill="#bcbec0"/>
						</svg>

                   	 	<h2 class="pt-lg-3">We are licensed and bonded for all your modification needs.</h2>
                        <p class="pt-lg-4">We at James Martin Contractor Services are committed to helping you achieve your goals in any construction  project. You dream it and we create it.</p>
                    </div>
                </div>
             	 <div class="col-lg-6 col-md-6">
                    <div class="team-info-bg">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Section 4-->
    <div class="section-construction pb-120" id="construction">
        <div class="services">
            <div class="item pt-100">
	           	<div class="container">
	                <div class="row">
					    <div class="col-md-6">
					    	<div class="thumbnail">
					    		<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/section-4/residential.png">
					    	</div>
					    </div>
					    <div class="col-md-6">
					    	<div class="desc">
					    		<h2>Residential</h2>
						    	<p class="pt-lg-4 pb-lg-1 w-75">From residential remodel to repair, from minor to major additions, James Martin Contractor Services is here to help.</p>
						    	<button class="btn btn-primary btn-item-request">Request an Estimate</button>
					    	</div>
					    </div>
					</div>
					<div class="row">
				   		<div class="col-md-4 pt-5">
				    		<button class="btn btn-primary btn-action w-100">New Construction</button>
					    </div>
					    <div class="col-md-4 pt-5">
					    	<button class="btn btn-primary btn-action w-100">Remodel</button>
					    </div>
					    <div class="col-md-4 pt-5">
					    	<button class="btn btn-primary btn-action w-100">Repair</button>
					    </div>
				    </div>
	            </div>
        		<div class="container pt-4">
			    	<ul class="list-inline d-flex justify-content-center m-0">
				    	<li>Homes</li>
				    	<li>Garages</li>
				    	<li>Bathrooms</li>
				    	<li>Basements</li>
				    	<li>Kitchens</li>
				    </ul>
				    <ul class="list-inline d-flex justify-content-center m-0">
				    	<li>Shiloh Cabinetry</li>
				    	<li>Formica Counter Tops</li>
				    	<li>Wilson Counter Tops</li>
				    </ul>
				    <ul class="list-inline d-flex justify-content-center m-0">
				    	<li>Sliding</li>
				    	<li>Roof</li>
				    	<li>Windows</li>
				    	<li>Polaris Windows</li>
				    </ul>
			    </div>
	        </div>
	        <div class="item pt-100">
	        	<div class="container">
	               <div class="row">
					    <div class="col-md-6">
					    	<div class="desc">
					    		<h2>Commercial</h2>
						    	<p class="pt-lg-4 pb-lg-1 w-75">No matter what your commercial building needs are, we handle any size project from concept, to design, to occupancy.</p>
						    	<button class="btn btn-primary btn-item-request">Request an Estimate</button>
					    	</div>
					    </div>
					    <div class="col-md-6">
					    	<div class="thumbnail">
					    		<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/section-4/commercial.png">
					    	</div>
					    </div>
					</div>
					<div class="row">  
				  		<div class="col-md-4 pt-5">
				    		<button class="btn btn-primary btn-action w-100">Design-build</button>
					    </div>
					    <div class="col-md-4 pt-5">
					    	<button class="btn btn-primary btn-action w-100">Construction Management</button>
					    </div>
					    <div class="col-md-4 pt-5">
					    	<button class="btn btn-primary btn-action w-100">General Construction</button>
					    </div>
				   </div>
	            </div>
	            <div class="container pt-4">
			    	<ul class="list-inline d-flex justify-content-center m-0">
				    	<li>Speciality Projects</li>
				    	<li>Bathrooms and Showers</li>
				    	<li>Building and Sliding</li>
				    	<li>Floors</li>
				    </ul>
				    <ul class="list-inline d-flex justify-content-center m-0">
				    	<li>Kitchen and Dining Remodel</li>
				    	<li>Laundry Facilities</li>
				    	<li>Office Remodels</li>
				    </ul>
				    <ul class="list-inline d-flex justify-content-center m-0">
				    	<li>Playstations and Counter Remodels</li>
				    	<li>Security Fencing</li>
				    	<li>Structural Works</li>
				    </ul>
			    </div>
	        </div>
	        <div class="item pt-100">
	           	<div class="container">
	                <div class="row">
					    <div class="col-md-6">
					    	<div class="thumbnail">
					    		<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/section-4/municipal.png">
					    	</div>
					    </div>
					    <div class="col-md-6">
					    	<div class="desc">
					    		<h2>Municipal</h2>
						    	<p class="pt-lg-4 pb-lg-1 w-75">As a fully integrated construction services company, James Martin Construction Services possesses the expertise and versatility needed to bring innovation and effiency to any project.</p>
						    	<button class="btn btn-primary btn-item-request">Learn More</button>
					    	</div>
					    </div>
					</div>	
	            </div>	
	        </div>
	        <div class="item pt-100 last-item">
	        	<div class="container">
	               <div class="row">
					    <div class="col-md-6">
					    	<div class="desc">
					    		<h2>Historical Restoration</h2>
						    	<p class="pt-lg-4 pb-lg-1 w-75">Historic renovation projects are very special opportunities to receive aging, often fragile buildings of major historical significance.</p>
						    	<button class="btn btn-primary btn-item-request">Learn More</button>
					    	</div>
					    </div>
					    <div class="col-md-6">
					    	<div class="thumbnail">
					    		<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/section-4/historical.png">
					    	</div>
					    </div>
					</div>		
	            </div>      
	        </div>
        </div>
    </div>

    <!-- Section 5-->
    <div class="page-section section-testimonial pb-120" id="testimonial">
		<div class="container">
            <div class="row">
                <div class="col-md-12">
                	<!--testiomnial-->
					<?php if ( is_active_sidebar( 'home-widget' ) ) : ?>
					    <?php dynamic_sidebar( 'home-widget' ); ?>
					<?php endif; ?>
                    <!--CAROUSEL-->             				
                	<div id="carouselExampleInterval" class="carousel slide" data-ride="carousel" style="display:none">
						<div class="carousel-inner">
						    <div class="carousel-item active" data-interval="10000">
							    <div class="testimonial-info text-center">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="145.049" height="25.695" viewBox="0 0 145.049 25.695">
									  <defs>
									    <clipPath id="clip-path">
									      <rect width="145.049" height="25.695" fill="none"/>
									    </clipPath>
									  </defs>
									  <g id="stars" clip-path="url(#clip-path)">
									    <g transform="translate(-667 -2021.203)">
									      <path id="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									    <g transform="translate(-638 -2021.203)">
									      <path id="icons8-filled_star-2" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									    <g transform="translate(-609 -2021.203)">
									      <path id="icons8-filled_star-3" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									    <g transform="translate(-580 -2021.203)">
									      <path id="icons8-filled_star-4" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									    <g transform="translate(-551 -2021.203)">
									      <path id="icons8-filled_star-5" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									  </g>
									</svg>

			                        <p>"Everything looks great. ..very pleased with everything! I did have him paint the self that goes around the middle of the room in the dover white trim color, and paint latte color under the shelf on both sides of the room. To have it all painted is well any extra you may need to charge. So appriaciate of your getting it done in such a timely manner.</p>
			                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/default-profile2.png">
			                       	<h5>Leslie M</h5>
			                       	<button class="btn btn-primary btn-more">More Testimonials</button>
		                        </div>
						    </div>
						    <div class="carousel-item" data-interval="10000">
							    <div class="testimonial-info text-center">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="145.049" height="25.695" viewBox="0 0 145.049 25.695">
									  <defs>
									    <clipPath id="clip-path">
									      <rect width="145.049" height="25.695" fill="none"/>
									    </clipPath>
									  </defs>
									  <g id="stars" clip-path="url(#clip-path)">
									    <g transform="translate(-667 -2021.203)">
									      <path id="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									    <g transform="translate(-638 -2021.203)">
									      <path id="icons8-filled_star-2" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									    <g transform="translate(-609 -2021.203)">
									      <path id="icons8-filled_star-3" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									    <g transform="translate(-580 -2021.203)">
									      <path id="icons8-filled_star-4" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									    <g transform="translate(-551 -2021.203)">
									      <path id="icons8-filled_star-5" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									  </g>
									</svg>

			                        <p>"Everything looks great. ..very pleased with everything! I did have him paint the self that goes around the middle of the room in the dover white trim color, and paint latte color under the shelf on both sides of the room. To have it all painted is well any extra you may need to charge. So appriaciate of your getting it done in such a timely manner.</p>
			                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/default-profile2.png">
			                       	<h5>Leslie M</h5>
			                       	<button class="btn btn-primary btn-more">More Testimonials</button>
		                        </div>
						    </div>
						    <div class="carousel-item" data-interval="10000">
							    <div class="testimonial-info text-center">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="145.049" height="25.695" viewBox="0 0 145.049 25.695">
									  <defs>
									    <clipPath id="clip-path">
									      <rect width="145.049" height="25.695" fill="none"/>
									    </clipPath>
									  </defs>
									  <g id="stars" clip-path="url(#clip-path)">
									    <g transform="translate(-667 -2021.203)">
									      <path id="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									    <g transform="translate(-638 -2021.203)">
									      <path id="icons8-filled_star-2" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									    <g transform="translate(-609 -2021.203)">
									      <path id="icons8-filled_star-3" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									    <g transform="translate(-580 -2021.203)">
									      <path id="icons8-filled_star-4" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									    <g transform="translate(-551 -2021.203)">
									      <path id="icons8-filled_star-5" data-name="icons8-filled_star" d="M12.263,4.051l2.863,5.794,6.4.928-4.631,4.509,1.093,6.368-5.724-3-5.724,3,1.093-6.368L3,10.773l6.4-.928Z" transform="translate(668.262 2021.2)" fill="#ffca28"/>
									    </g>
									  </g>
									</svg>

			                        <p>"Everything looks great. ..very pleased with everything! I did have him paint the self that goes around the middle of the room in the dover white trim color, and paint latte color under the shelf on both sides of the room. To have it all painted is well any extra you may need to charge. So appriaciate of your getting it done in such a timely manner.</p>
			                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/default-profile2.png">
			                       	<h5>Leslie M</h5>
			                       	<button class="btn btn-primary btn-more">More Testimonials</button>
		                        </div>
						    </div>
						</div>
						 <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
					</div>
                </div>
            </div>
        </div>
    </div>
    <!-- Section 6-->
    <div class="section-expertise pt-120 pb-120" id="expertise">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="expertise-info">
						<svg id="logo" xmlns="http://www.w3.org/2000/svg" width="77.858" height="66.147" viewBox="0 0 77.858 66.147">
						  <path id="Path_19" data-name="Path 19" d="M111.907,75.457h7.6L91.335,124.238H83.661Z" transform="translate(-60.276 -58.091)" fill="#bfa665"/>
						  <path id="Path_20" data-name="Path 20" d="M142.455,75.457h7.6l-28.174,48.782h-7.674Z" transform="translate(-80.051 -58.091)" fill="#bfa665"/>
						  <path id="Path_21" data-name="Path 21" d="M80.44,75.457h7.6L59.869,124.238H52.195Z" transform="translate(-39.906 -58.091)" fill="#bfa665"/>
						  <g id="Group_2" data-name="Group 2" transform="translate(0)">
						    <path id="Path_22" data-name="Path 22" d="M77.073,26.213h7.6L56.5,75H48.827Z" transform="translate(-37.726 -26.213)" fill="#fff" opacity="0.8"/>
						    <path id="Path_23" data-name="Path 23" d="M36.922,106.157l-11.9,20.6H17.347l11.922-20.6Z" transform="translate(-17.347 -77.966)" fill="#fff" opacity="0.8"/>
						    <path id="Path_24" data-name="Path 24" d="M146.579,33.953,141.4,42.8l1.239,2.75h10.209Z" transform="translate(-97.653 -31.224)" fill="rgba(255,255,255,0.5)"/>
						  </g>
						  <path id="Path_25" data-name="Path 25" d="M210.846,84.727l-5.179,8.846,1.239,2.75h10.209Z" transform="translate(-139.257 -64.093)" fill="rgba(255,255,255,0.5)"/>
						</svg>
                   	 	<h2 class="pt-lg-3">The expertise and versatility needed to bring innovation.</h2>
                        <p class="pt-lg-3">Your partner construction and facility management for companies that need remodeling or building repairs.</p>
                        <button class="btn btn-primary btn-estimate">Request an Estimate</button>
                    </div>
                </div>
             	 <div class="col-md-6">
             	 	<div class="thumbnail">
             	 		<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/section-6/img1.png">
             	 	</div>
                </div>
            </div>
            <div class="expertise-contact pt-100 text-center">
            	<div class="row">
                	<div class="col-md-4">
                		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="32" height="46" viewBox="0 0 32 46">
						  <defs>
						    <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="2.487" gradientUnits="objectBoundingBox">
						      <stop offset="0" stop-color="#bfa665"/>
						      <stop offset="1" stop-color="#bfa665" stop-opacity="0"/>
						    </linearGradient>
						  </defs>
						  <path id="Path_87" data-name="Path 87" d="M25,2A16.021,16.021,0,0,0,9,18C9,30.078,23.629,46.945,24.25,47.656A.992.992,0,0,0,25,48a1.006,1.006,0,0,0,.75-.344C26.371,46.945,41,30.152,41,18A16.021,16.021,0,0,0,25,2Zm0,9.844,8,4.813-1,1.688V26H18V18.344l-1-1.687Zm0,2.313-5,3V24h3V19h4v5h3V17.156Z" transform="translate(-9 -2)" fill="url(#linear-gradient)"/>
						</svg>
                		<p class="address pt-4">3649 Lane Road Extension</p>
                		<p class="address">Suite 301, Perry, Ohio 44081</p>
                	</div>
                	<div class="col-md-4">
                		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="43.998" height="44" viewBox="0 0 43.998 44">
						  <defs>
						    <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="2.606" gradientUnits="objectBoundingBox">
						      <stop offset="0" stop-color="#bfa665"/>
						      <stop offset="1" stop-color="#bfa665" stop-opacity="0"/>
						    </linearGradient>
						  </defs>
						  <path id="icon_-_phone_blue" data-name="icon - phone blue" d="M11.551,47a3.543,3.543,0,0,1-2.094-.711C9.395,46.242,3,41.563,3,39l.008-.152C3.3,32.8,7.875,25.313,16.594,16.594s16.2-13.285,22.25-13.586l.1,0c.027,0,.055,0,.082,0,2.617,0,7.215,6.395,7.262,6.457a3.055,3.055,0,0,1-.32,4.281c-.7.527-8.09,5.367-8.992,5.906a3.48,3.48,0,0,1-3.48-.262c-.7-.383-2.895-1.637-3.91-2.223A48.149,48.149,0,0,0,23,22.988a55.159,55.159,0,0,0-5.836,6.605c.59,1.023,1.84,3.211,2.234,3.938a3.456,3.456,0,0,1,.219,3.484c-.508.848-5.437,8.332-5.883,8.949a2.582,2.582,0,0,1-1.855,1.016A2.868,2.868,0,0,1,11.551,47Z" transform="translate(-3 -3)" fill="url(#linear-gradient)"/>
						</svg>

                		<p class="tel pt-4">440-392-6088</p>
                	</div>
                	<div class="col-md-4">
                		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="46" height="46" viewBox="0 0 46 46">
						  <defs>
						    <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="3.234" gradientUnits="objectBoundingBox">
						      <stop offset="0" stop-color="#bfa665"/>
						      <stop offset="1" stop-color="#bfa665" stop-opacity="0"/>
						    </linearGradient>
						  </defs>
						  <path id="icons8-clock_filled" d="M25,2A23,23,0,1,0,48,25,23.025,23.025,0,0,0,25,2Zm0,26a2.969,2.969,0,0,1-1.286-.3l-6.007,6.007a1,1,0,0,1-1.414-1.414L22.3,26.286a2.964,2.964,0,0,1,1.7-4.1V8a1,1,0,0,1,2,0V22.184A2.993,2.993,0,0,1,25,28Z" transform="translate(-2 -2)" fill="url(#linear-gradient)"/>
						</svg>

                		<p class="working-hour pt-4">M - F 8am - 5pm</p>
                	</div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();