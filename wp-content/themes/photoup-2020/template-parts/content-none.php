<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package PhotoUp 2020
 */

?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'photoup-2020' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
	
		<p><?php esc_html_e( 'It looks like nothing was found at this location.', 'photoup-2020' ); ?></p>
			
	</div><!-- .page-content -->
</section><!-- .no-results -->
