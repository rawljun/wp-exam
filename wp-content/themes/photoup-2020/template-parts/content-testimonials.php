<?php
/**
 * Template part for displaying testimonials in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package PhotoUp 2020
 */


	$profile_id = get_post_meta(get_the_ID(), '_testimonial_author_profile', true);
	$profile 	= wp_get_attachment_image_src( $profile_id, 'full' );
    $rating 	= get_post_meta( get_the_ID(), '_testimonial_rating', true );
	?>
	<div class="row">
		<div class="col-md-12">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="145.049" height="25.695" viewBox="0 0 145.049 25.695">
			  <defs>
			    <clipPath id="clip-path">
			      <rect width="145.049" height="25.695" fill="none"/>
			    </clipPath>
			  </defs>
			  <g id="stars" clip-path="url(#clip-path)">
			  	<?php apply_filters( 'star_rating', $rating ); ?>
			  </g>
			</svg>
	        <p><?php  echo get_the_content(); ?></p>
	       	<h5><?php echo  get_post_meta(get_the_ID(), '_testimonial_author', true) ?></h5>
		</div>
    </div>